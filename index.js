const tmi = require("tmi.js");
const discord = require("discord.js");
const dcclient = new discord.Client();

const twclient = new tmi.Client(require("./twitchconfig"));

let buhtimeout = false;
let lastbuh = "";
let buhmanualdeactivated = false;
let dcchannelconnection = null;

const prefix = "!";

twclient.connect();

twclient.on("message", (channel, tags, message, self) => {
  if (self) return;
  if (channel != "#fatifer91") return;
  if (
    message[0] == prefix &&
    message
      .split(prefix)
      .slice(-1)[0]
      .toLowerCase()
      .split(" ")
      .some(el => el == "buh")
  ) {
    if (!buhtimeout && !buhmanualdeactivated) {
      buhtimeout = true;
      lastbuh = new Date();
      twclient.say(channel, `@${tags.username} Tu das nie wieder!`);
      if (dcchannelconnection != null) {
        const fs = require("fs");
        const stream = fs.createReadStream("./buh.mp3");
        const dispatcher = dcchannelconnection.playStream(stream);

        dispatcher.setVolume(0.6);

        setTimeout(() => {
          dcchannelconnection.disconnect();

          setTimeout(() => {
            const liveStreamChannels = dcclient.channels.filter(
              channel =>
                channel instanceof discord.VoiceChannel &&
                channel.name.toLowerCase().includes("live stream")
            );
            liveStreamChannels
              .first()
              .join()
              .then(connection => {
                dcchannelconnection = connection;
              });
          }, 1000);
        }, 3500);
      }

      setTimeout(() => {
        buhtimeout = false;
      }, 600000);
    } else {
      twclient.say(
        channel,
        `@${tags.username} Die "Buh" Funktion ist temporär deaktiviert. ${
          buhmanualdeactivated
            ? "Fatifer wurde zu oft genervt."
            : `Noch ${Math.round(
                (600 - (new Date().valueOf() - lastbuh.valueOf()) / 1000) / 60
              )} Minuten. `
        }`
      );
    }
  }
  if (
    message[0] == prefix &&
    message
      .split(prefix)
      .slice(-1)[0]
      .toLowerCase()
      .split(" ")
      .some(el => el == "togglebuh")
  ) {
    if (
      tags.mod ||
      (tags.badges != null &&
        tags.badges.broadcaster != null &&
        tags.badges.broadcaster == "1")
    ) {
      buhmanualdeactivated = !buhmanualdeactivated;
      twclient.say(
        channel,
        buhmanualdeactivated
          ? `@${tags.username} Die "Buh" Funktion wurde deaktiviert.`
          : `@${tags.username} Die "Buh" Funktion wurde aktiviert.`
      );
    } else {
      twclient.say(
        channel,
        `@${tags.username} Das Kommando ist nur für Moderatoren verfügbar.`
      );
    }
  }
  if (
    message[0] == prefix &&
    message
      .split(prefix)
      .slice(-1)[0]
      .toLowerCase()
      .split(" ")
      .some(el => el == "disablebuh")
  ) {
    if (
      tags.mod ||
      (tags.badges != null &&
        tags.badges.broadcaster != null &&
        tags.badges.broadcaster == "1")
    ) {
      buhmanualdeactivated = true;
      twclient.say(
        channel,
        `@${tags.username} Die "Buh" Funktion wurde deaktiviert.`
      );
    } else {
      twclient.say(
        channel,
        `@${tags.username} Das Kommando ist nur für Moderatoren verfügbar.`
      );
    }
  }
  if (
    message[0] == prefix &&
    message
      .split(prefix)
      .slice(-1)[0]
      .toLowerCase()
      .split(" ")
      .some(el => el == "enablebuh")
  ) {
    if (
      tags.mod ||
      (tags.badges != null &&
        tags.badges.broadcaster != null &&
        tags.badges.broadcaster == "1")
    ) {
      buhmanualdeactivated = false;
      twclient.say(
        channel,
        `@${tags.username} Die "Buh" Funktion wurde aktiviert.`
      );
    } else {
      twclient.say(
        channel,
        `@${tags.username} Das Kommando ist nur für Moderatoren verfügbar.`
      );
    }
  }
  if (
    message[0] == prefix &&
    message
      .split(prefix)
      .slice(-1)[0]
      .toLowerCase()
      .split(" ")
      .some(el => el == "buhstatus")
  ) {
    console.log(tags);
  }
  if (
    message[0] == prefix &&
    message
      .split(prefix)
      .slice(-1)[0]
      .toLowerCase()
      .split(" ")
      .some(el => el == "resetbuh")
  ) {
    if (
      tags.mod ||
      (tags.badges != null &&
        tags.badges.broadcaster != null &&
        tags.badges.broadcaster == "1")
    ) {
      buhtimeout = false;
    }
  }
});

dcclient.on("message", () => {});

dcclient.on("ready", () => {
  console.log(`logged in as: ${dcclient.user.tag}`);
  const liveStreamChannels = dcclient.channels.filter(
    channel =>
      channel instanceof discord.VoiceChannel &&
      channel.name.toLowerCase().includes("live stream")
  );
  liveStreamChannels
    .first()
    .join()
    .then(connection => {
      dcchannelconnection = connection;
    });
});

dcclient.login("NjM3NjMyNzg1OTUxMjI3OTM1.XbRAkw.j2KXmj3lXIGP8OJ8F0E6KRWCzMc");
